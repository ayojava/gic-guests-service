package org.javasoft.gicguests.service

import org.javasoft.gicguests.entity.SmsMessageEntity

interface SmsMessageServiceIntf {

    fun saveSMS(smsMessageEntity: SmsMessageEntity) : SmsMessageEntity

    fun findAllPendingSMSAlert() : MutableList<SmsMessageEntity>

    fun saveAll(allSmsMessages : MutableList<SmsMessageEntity>)

    fun deleteSMS(guesId : String)
}