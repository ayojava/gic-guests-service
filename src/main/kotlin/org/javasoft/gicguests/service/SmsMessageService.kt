package org.javasoft.gicguests.service

import org.javasoft.gicguests.entity.SmsMessageEntity
import org.javasoft.gicguests.repository.SmsMessageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

const val PENDING : String ="P"

const val SENT : String ="S"

@Service
class SmsMessageService : SmsMessageServiceIntf{

    @Autowired
    lateinit var smsMessageRepository: SmsMessageRepository

    override fun findAllPendingSMSAlert(): MutableList<SmsMessageEntity> {
        return smsMessageRepository.findAllByStatus(PENDING)
    }

    override fun saveSMS(smsMessageEntity: SmsMessageEntity): SmsMessageEntity
            = smsMessageRepository.save(smsMessageEntity)

    override fun saveAll(allSmsMessages: MutableList<SmsMessageEntity>) {
        smsMessageRepository.saveAll(allSmsMessages)
    }


    override fun deleteSMS(guestId : String){
        smsMessageRepository.findByGuestId(guestId).ifPresent {
            smsMessageRepository.delete(it)
        }
    }
}