package org.javasoft.gicguests.service

import org.javasoft.gicguests.entity.ProgramEntity
import org.javasoft.gicguests.repository.ProgramRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProgramService{

    @Autowired
    lateinit var programRepository: ProgramRepository

    fun saveProgram(programEntity: ProgramEntity) : ProgramEntity = programRepository.save( programEntity)

    fun listOfPrograms() : MutableList<ProgramEntity> = programRepository.findAll();

    fun findProgramEntity(serviceName : String , serviceType : String) : ProgramEntity = programRepository.findByServiceNameAndServiceType(serviceName,serviceType)

    fun findProgramListByStatus()  : MutableList<ProgramEntity> =programRepository.findByStatusOrderByServiceTypeAscServiceNameAsc(status = true)

}