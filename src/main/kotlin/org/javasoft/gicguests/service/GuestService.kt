package org.javasoft.gicguests.service

import org.javasoft.gicguests.entity.GuestEntity
import org.javasoft.gicguests.repository.GuestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.*

@Service
class GuestService{

    @Autowired
    lateinit var guestRepository: GuestRepository;

    fun saveGuestEntity(guestEntity: GuestEntity): GuestEntity = guestRepository.save(guestEntity)

    fun findAllGuest() :  MutableList<GuestEntity> =  guestRepository.findAll(Sort(Sort.Direction.DESC, "attendanceDate"))

    fun findAGuest(guestId : String): GuestEntity = guestRepository.findById(guestId).get()

    fun findAGuestOpt(guestId : String):Optional<GuestEntity> = guestRepository.findById(guestId)

    fun deleteAGuestById(guestId : String) = guestRepository.deleteById(guestId)

}