package org.javasoft.gicguests

import java.security.MessageDigest

fun String.hashWithSHA512():String {
    val digest = MessageDigest.getInstance("SHA-512")
    val bytes = digest.digest(this.toByteArray(Charsets.UTF_8))
    return bytes.fold("", { str, it -> str + "%02x".format(it) })
}

fun String.toInternationalFormat() : String= "234" +this.substring(1)
