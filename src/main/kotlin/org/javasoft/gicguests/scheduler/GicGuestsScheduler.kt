package org.javasoft.gicguests.scheduler

import org.javasoft.gicguests.config.EbulkSmsConfig
import org.javasoft.gicguests.facade.SmsFacade
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Component
@Transactional
class SmsAlertScheduler{

    @Autowired
    lateinit var ebulkSmsConfig: EbulkSmsConfig;

    @Autowired
    lateinit var ebulkSmsFacade: SmsFacade;

    @Scheduled(cron = "\${smsCronExpression}")
    fun smsScheduler(){
        log.info("The time is now ${DateTimeFormatter.ISO_LOCAL_TIME.format(LocalDateTime.now())}")
        ebulkSmsFacade.checkAndSendPendingSMS()
    }

    companion object {
        val log = LoggerFactory.getLogger(SmsAlertScheduler::class.java)
    }
}