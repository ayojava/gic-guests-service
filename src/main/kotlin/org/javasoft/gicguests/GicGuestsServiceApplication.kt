package org.javasoft.gicguests

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@SpringBootApplication
class GicGuestsServiceApplication

fun main(args: Array<String>) {
    runApplication<GicGuestsServiceApplication>(*args)
}
