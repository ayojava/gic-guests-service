package org.javasoft.gicguests.repository

import org.javasoft.gicguests.entity.SystemUserEntity
import org.springframework.data.mongodb.repository.MongoRepository

interface SystemUserRepository : MongoRepository<SystemUserEntity, String> {

    fun findByUserNameAndPassword(userName : String,password : String):SystemUserEntity
}