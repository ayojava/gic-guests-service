package org.javasoft.gicguests.repository
import org.javasoft.gicguests.entity.SmsMessageEntity
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*


interface SmsMessageRepository : MongoRepository<SmsMessageEntity,String> {

    fun findAllByStatus(status : String) : MutableList<SmsMessageEntity>

    fun findByGuestId(guestId : String) : Optional<SmsMessageEntity>
}