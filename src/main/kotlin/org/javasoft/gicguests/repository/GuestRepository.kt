package org.javasoft.gicguests.repository

import org.javasoft.gicguests.entity.GuestEntity
import org.springframework.data.mongodb.repository.MongoRepository

interface GuestRepository : MongoRepository<GuestEntity, String> {
}