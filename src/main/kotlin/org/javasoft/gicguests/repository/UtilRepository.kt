package org.javasoft.gicguests.repository

import org.javasoft.gicguests.entity.ProgramEntity
import org.springframework.data.mongodb.repository.MongoRepository

interface ProgramRepository : MongoRepository<ProgramEntity, String> {

    fun findByServiceNameAndServiceType(serviceName : String , serviceType : String): ProgramEntity

    fun findByStatusOrderByServiceTypeAscServiceNameAsc(status : Boolean) : MutableList<ProgramEntity>
}