package org.javasoft.gicguests.api

const val SYSTEM_USER_V1_API="/v1/systemUser"

const val GUEST_V1_API="/v1/guest"

const val UTIL_V1_API="/v1/util"

const val  LOGIN_API="/login/{userName}/{password}"

const val A_GUEST_API="/aGuest/{guestId}";