package org.javasoft.gicguests.facade

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.javasoft.gicguests.config.EbulkSmsConfig
import org.javasoft.gicguests.entity.SmsMessageEntity
import org.javasoft.gicguests.enums.EbulkStatusEnum
import org.javasoft.gicguests.payload.*
import org.javasoft.gicguests.service.SENT
import org.javasoft.gicguests.service.SmsMessageServiceIntf
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.*
import org.springframework.http.MediaType.*
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import java.time.LocalDateTime
import kotlin.math.roundToInt

@Component
class SmsFacade(private val restTemplate: RestTemplate){

    @Autowired
    lateinit var ebulkSmsConfig: EbulkSmsConfig;

    @Autowired
    lateinit var smsMessageService: SmsMessageServiceIntf

    @Autowired
    lateinit var emailFacade: EmailFacade

    fun saveSmsRequestPayload(smsRequestPayload: SmsRequestPayload) : SmsResponsePayload{
        val smsMessageEntity = SmsRequestPayload.createSmsMessageEntity(smsRequestPayload)
        val entity = smsMessageService.saveSMS(smsMessageEntity)
        return SmsResponsePayload.createSmsResponsePayload(entity)
    }

    fun checkAndSendPendingSMS(){
        var pendingSMSAlert = smsMessageService.findAllPendingSMSAlert()

        val pendingSms = pendingSMSAlert.size

        val smsBalance = restTemplate.getForObject(ebulkSmsConfig.ebulkGetBalanceUrl,Double::class.java)!!.roundToInt()
        log.info("Pending SMS Alert ::: ${pendingSms}    SmsBalance ::: ${smsBalance}")
        when{
            pendingSms == 0 -> return
            smsBalance == 0 -> "Send email"
            smsBalance > pendingSms * 2 -> sendIndividualSMS(pendingSMSAlert)
            smsBalance < pendingSms * 2 -> "Inform sola that we have less than enough sms"
        }
    }

    fun sendIndividualSMS(smsMessageEntityList: MutableList<SmsMessageEntity>){
        log.info(":::::: Sending sms to recipients :::::: ")

        var auth = Auth(ebulkSmsConfig.userEmail, ebulkSmsConfig.authkey)
        val header = HttpHeaders();
        header.contentType = APPLICATION_JSON

        smsMessageEntityList.forEach { it->
            run {
                val recipientMsg = RecipientMsg(it.phoneNo, it.id!!)
                val aRecipient = Recipients(mutableListOf(recipientMsg))
                var smsMessage = SmsMessage(senderName = ebulkSmsConfig.senderName, messageText = it.smsMessage)

                var SMS = EbulkSmsRequest(SmsRequest(auth, smsMessage, aRecipient))
                log.info("\n\n::::::::: SMS ::::: ${jacksonObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(SMS)}\n\n")

                var httpEntity = HttpEntity<EbulkSmsRequest>(SMS, header)
                var exchange: ResponseEntity<EbulkSmsResponse> = restTemplate.exchange(ebulkSmsConfig.ebulkSendSmsUrl, HttpMethod.POST, httpEntity, EbulkSmsResponse::class.java)
                val ebulkSmsResponse = exchange.body

                val statusMsg = ebulkSmsResponse?.statusMsg
                log.info("Status :::: $statusMsg")
                val ebulkStatusEnum = EbulkStatusEnum.getEnum(statusMsg?.status!!)
                when{
                    ebulkStatusEnum.result -> smsMessageService.saveSMS(it.copy(status = SENT,sentDate = LocalDateTime.now()))
                    else-> return
                }
            }
        }


    }

    /*
    fun sendSms(smsMessageEntityList: MutableList<SmsMessageEntity>){
        log.info(":::::: Sending sms to recipients :::::: ")
        var recipientMsgs = mutableListOf<RecipientMsg>()
        var mapFn : (SmsMessageEntity) -> RecipientMsg ={it -> RecipientMsg(it.phoneNo,it.id!!)}

        val mapTo: MutableList<RecipientMsg> = smsMessageEntityList.mapTo(recipientMsgs, mapFn)


        var recipients = Recipients(mapTo)

        var auth = Auth(ebulkSmsConfig.userEmail, ebulkSmsConfig.authkey)
        var smsMessage = SmsMessage(senderName = ebulkSmsConfig.senderName, messageText = "Thanks for coming to service today")


        var SMS = EbulkSmsRequest(SmsRequest (auth, smsMessage, recipients))

        val mapper = jacksonObjectMapper()

        //log.info("\n\n::::::::: SMS ::::: ${mapper.writerWithDefaultPrettyPrinter().writeValueAsString(SMS)}\n\n")

        val header = HttpHeaders();
        header.contentType = APPLICATION_JSON

        var httpEntity = HttpEntity<EbulkSmsRequest>(SMS, header)


        var exchange: ResponseEntity<EbulkSmsResponse> = restTemplate.exchange(ebulkSmsConfig.ebulkSendSmsUrl, HttpMethod.POST, httpEntity, EbulkSmsResponse::class.java)
        val ebulkSmsResponse = exchange.body

        var transformFn : (SmsMessageEntity) -> SmsMessageEntity = { it -> SmsMessageEntity(id=it.id,phoneNo =  it.phoneNo,createDate =  it.createDate,status =  SENT, sentDate = LocalDateTime.now(), smsMessage = it.smsMessage) }
        var transformSmsMessageEntity: List<SmsMessageEntity> = smsMessageEntityList.map(transformFn)
        smsMessageService.saveAll(transformSmsMessageEntity.toMutableList())
    }

*/
    companion object {
        val log = LoggerFactory.getLogger(SmsFacade::class.java)
    }

}