package org.javasoft.gicguests.facade

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMailMessage
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Component

@Component
class EmailFacade(private val javaMailSender: JavaMailSender){

    fun sendEmail(smsBalance: Int ){
        val mimeMessage = javaMailSender.createMimeMessage()
        val mimeMessageHelper = MimeMessageHelper(mimeMessage)

        mimeMessageHelper.setTo("ayojava@gmail.com")
        mimeMessageHelper.setText("You Do not have enough sms credits on your account. Your current balance is " + smsBalance)
        mimeMessageHelper.setSubject("Insufficient SMS Units")

        javaMailSender.send(mimeMessage)
    }
}