package org.javasoft.gicguests.facade

import org.javasoft.gicguests.entity.toCreateSystemUserResponse
import org.javasoft.gicguests.entity.toLoginSystemUserResponse
import org.javasoft.gicguests.payload.CreateSystemUserRequest
import org.javasoft.gicguests.payload.CreateSystemUserResponse
import org.javasoft.gicguests.payload.LoginSystemUserResponse
import org.javasoft.gicguests.payload.toEntity
import org.javasoft.gicguests.repository.SystemUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class SystemUserFacade (@Autowired val systemUserRepository: SystemUserRepository){

    fun createSystemUser(createSystemUserRequestPayload: CreateSystemUserRequest) : CreateSystemUserResponse{
        val systemUserEntity = createSystemUserRequestPayload.toEntity()
        return systemUserRepository.save(systemUserEntity).toCreateSystemUserResponse()
    }

    fun loginSystemUser(userName :String , password : String) : LoginSystemUserResponse{
        val systemUserEntity = systemUserRepository.findByUserNameAndPassword(userName, password)
        return systemUserEntity.toLoginSystemUserResponse()
    }
}