package org.javasoft.gicguests.facade

import org.apache.commons.lang.StringUtils
import org.javasoft.gicguests.config.EbulkSmsConfig
import org.javasoft.gicguests.config.ValidateConfig
import org.javasoft.gicguests.entity.GuestEntity
import org.javasoft.gicguests.entity.PrayerRequestEntity
import org.javasoft.gicguests.entity.SmsMessageEntity
import org.javasoft.gicguests.payload.*
import org.javasoft.gicguests.service.GuestService
import org.javasoft.gicguests.service.ProgramService
import org.javasoft.gicguests.service.SmsMessageServiceIntf
import org.javasoft.gicguests.toInternationalFormat
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.sql.Date
import java.time.ZoneId

@Component
class GuestFacade(private val validateConfig: ValidateConfig){

    @Autowired
    lateinit var guestService: GuestService

    @Autowired
    lateinit var ebulkSmsConfig: EbulkSmsConfig

    @Autowired
    lateinit var programService: ProgramService

    @Autowired
    lateinit var smsMessageService: SmsMessageServiceIntf

    fun newGuest(newGuestRequest: NewGuestRequest): NewGuestResponse{
       return  when{
            validateConfig.validateString(clearString = newGuestRequest.toClearString(), hashString = newGuestRequest.hash) -> handleNewGuest(newGuestRequest)
            else -> NewGuestResponse(responseCode = "GIC-01",responseDesc = "Invalid Hash",guestId = StringUtils.EMPTY)
        }
    }

    fun editGuestDetails(editGuestRequest: EditGuestRequest) : DefaultResponse{
        val (guestId, bioData, prayerData, programData, hash, resendSMS) = editGuestRequest
        guestService.findAGuestOpt(guestId).map { guestEntityDB ->

            val (serviceName, _, serviceType) = programData
            val newProgramEntity = programService.findProgramEntity(serviceName , serviceType)

            val(prayerRequest1 ,prayerRequest2,prayerRequest3,prayerRequest4) = prayerData
            val prayerRequestEntity = PrayerRequestEntity(prayerRequest1 = prayerRequest1 , prayerRequest2 = prayerRequest2,
                    prayerRequest3 = prayerRequest3, prayerRequest4 = prayerRequest4)

            val guestEntityCopy = guestEntityDB.copy(otherNames = bioData.otherNames, surname = bioData.surname, gender = bioData.gender, maritalStatus = bioData.maritalStatus,
                    ageRange = bioData.ageRange, email = bioData.email, altEmail = bioData.altEmail, birthday = bioData.birthday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                    contactAddress = bioData.contactAddress, nearestBstop = bioData.nearestBstp, category = bioData.category,
                    attendanceDate = programData.programDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                    prayerRequestEntity = prayerRequestEntity,altPhoneNo = bioData.altPhoneNo,phoneNo = bioData.phoneNo ,
                    programEntity = newProgramEntity)

            guestService.saveGuestEntity(guestEntityCopy)

            when{
                resendSMS -> {
                    val newPhoneNo = guestEntityCopy.phoneNo.toInternationalFormat()
                    val newSmsMessage = String.format(ebulkSmsConfig.welcomeMessage,guestEntityCopy.surname +" " + guestEntityCopy.otherNames)
                    smsMessageService.saveSMS(SmsMessageEntity(phoneNo = newPhoneNo , smsMessage = newSmsMessage ,guestId = guestEntityCopy.id!!))
                }
                else -> "Nothing"
            }

        }

        return DefaultResponse(responseCode = "00" ,responseDesc = "Guest Details successfully updated")
    }

    fun handleNewGuest(newGuestRequest: NewGuestRequest): NewGuestResponse {
        val (guestEntity, smsMessageEntity) = newGuestRequest.toEntity()
//        val churchService = newGuestRequest.programData.churchService
//        val programName = .programName
        val (serviceName, _, serviceType) = newGuestRequest.programData
        //log.info("\nService Name =========== $serviceName , Service Type ============== $serviceType")
        val programEntity = programService.findProgramEntity(serviceName , serviceType)
       // log.info("\nProgram Entity =========== $programEntity ")
        guestEntity.programEntity = programEntity

        val fullName = guestEntity.surname +" " + guestEntity.otherNames

        //smsMessageEntity.smsMessage =
        val entity = guestService.saveGuestEntity(guestEntity)

        val smsMessageCopy = smsMessageEntity.copy(smsMessage = String.format(ebulkSmsConfig.welcomeMessage, fullName) , guestId = entity.id!!)
        smsMessageService.saveSMS(smsMessageCopy)

        return NewGuestResponse(guestId = entity.id!!)
    }

    fun getAllGuests() : MutableList<GuestDataResponse> =guestService.findAllGuest().map { guestEntity ->mapGuestEntity(guestEntity)}.toMutableList()

    fun findAGuest(guestId : String ) : GuestDataResponse = mapGuestEntity(guestService.findAGuest(guestId))


    fun mapGuestEntity(guestEntity: GuestEntity) : GuestDataResponse{
        val (id, otherNames, surname, gender, maritalStatus, ageRange, email, altEmail, phoneNo, altPhoneNo, birthday, attendanceDate
                , contactAddress, nearestBstop, category, prayerRequestEntity, programEntity) = guestEntity

        val bioData:BioData = BioData(surname = surname,otherNames = otherNames,gender = gender,maritalStatus = maritalStatus,
                phoneNo = phoneNo,altPhoneNo = altPhoneNo,email = email,altEmail = altEmail,
                birthday =Date.valueOf(birthday) ,category = category , contactAddress = contactAddress,ageRange = ageRange
                ,nearestBstp = nearestBstop  )

        val(_,prayerRequest1,prayerRequest2,prayerRequest3,prayerRequest4) = prayerRequestEntity

        val prayerData : PrayerData = PrayerData(prayerRequest1, prayerRequest2, prayerRequest3, prayerRequest4)

        val(entityId,serviceName,serviceType,status) = programEntity!!
        val programData : ProgramData = ProgramData(serviceName = serviceName , serviceType = serviceType,
                programDate = Date.valueOf(attendanceDate) )

        return GuestDataResponse(guestId =id!! , bioData = bioData,prayerData = prayerData,programData = programData,hash = "xxxxxx")
    }

    fun deleteAGuest(guestId: String) : DefaultResponse{
        guestService.deleteAGuestById(guestId);
        smsMessageService.deleteSMS(guestId)
        //phoneNo.toInternationalFormat()
        return DefaultResponse(responseCode = "00" ,responseDesc = "Guest Details successfully deleted")
    }

    companion object {
        val log = LoggerFactory.getLogger(GuestFacade::class.java)
    }
}