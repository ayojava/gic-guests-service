package org.javasoft.gicguests.facade

import org.javasoft.gicguests.entity.ProgramEntity
import org.javasoft.gicguests.payload.NewProgramRequest
import org.javasoft.gicguests.payload.NewProgramResponse
import org.javasoft.gicguests.service.ProgramService
import org.springframework.stereotype.Component

@Component
class ProgramFacade(private val programService: ProgramService){

    fun saveProgram(newProgramRequest: NewProgramRequest) : NewProgramResponse{
        val (serviceName, serviceType) = newProgramRequest
        val programEntity = programService.saveProgram(programEntity = ProgramEntity(serviceName = serviceName, serviceType = serviceType))
        return NewProgramResponse(id = programEntity.id!! ,serviceType = programEntity.serviceType,serviceName = programEntity.serviceName)
    }

    fun listOfPrograms() : MutableList<NewProgramResponse> =
            programService.listOfPrograms().map { it-> NewProgramResponse(it.id!!,it.serviceName,it.serviceType) }.toMutableList()

    fun listOfActivePrograms() : MutableList<NewProgramResponse> =
            programService.findProgramListByStatus().map { it-> NewProgramResponse(it.id!!,it.serviceName,it.serviceType) }.toMutableList()
}