package org.javasoft.gicguests.controller

import io.swagger.annotations.Api
import org.javasoft.gicguests.facade.SmsFacade
import org.javasoft.gicguests.payload.SmsRequestPayload
import org.javasoft.gicguests.payload.SmsResponsePayload
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
//@RequestMapping("/gic")
@Api(value = "admin", description = "Rest API for administrative operations", tags = arrayOf("Admin API"))
class SmsController(@Autowired val smsFacade: SmsFacade){

    @PostMapping("/newSms")
    fun createSmsMessage(@RequestBody smsRequestPayload: SmsRequestPayload) : SmsResponsePayload{
        log.info("====Create SMS request Payload ==== {}",smsRequestPayload)
        return smsFacade.saveSmsRequestPayload(smsRequestPayload)
    }

    companion object {
        val log = LoggerFactory.getLogger(SmsController::class.java)
    }
}
//https://github.com/jungho/kotlin-spring-rest-mongodb/blob/master/src/main/kotlin/ca/architech/api/BlogController.kt
//http://javasampleapproach.com/spring-framework/spring-boot/kotlin-spring-boot/kotlin-springboot-mongodb-model-one-one-one-many-relationships-embedded-documents
//http://javasampleapproach.com/spring-framework/spring-boot/kotlin-spring-boot/kotlin-springdata-mongorepository-interact-mongodb
//https://medium.com/@rhwolniewicz/building-a-rest-service-with-kotlin-spring-boot-mongodb-and-junit-14d10faa594b