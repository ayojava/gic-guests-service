package org.javasoft.gicguests.controller

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.javasoft.gicguests.api.A_GUEST_API
import org.javasoft.gicguests.api.GUEST_V1_API
import org.javasoft.gicguests.facade.GuestFacade
import org.javasoft.gicguests.payload.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(GUEST_V1_API)
@Api(value ="GuestController" ,description = "Set of endpoints for Guests.")
class GuestController(@Autowired val guestFacade: GuestFacade){

    @PostMapping("/newGuest")
    @ApiOperation(value = "Post Request For New Guest", response = NewGuestResponse::class)
    fun newGuest(@ApiParam("NewGuestRequest Param") @RequestBody newGuestRequest: NewGuestRequest): NewGuestResponse{
        log.info("\n====NewGuestRequest ==== {}\n",newGuestRequest)
        return guestFacade.newGuest(newGuestRequest)
    }

    @PostMapping("/editGuest")
    @ApiOperation(value = "Post Request For Edit Guest", response = DefaultResponse::class)
    fun editGuest(@ApiParam("EditGuestRequest Param") @RequestBody editGuestRequest: EditGuestRequest): DefaultResponse{
        log.info("\n====EditGuestRequest ==== {}\n",editGuestRequest)
        return guestFacade.editGuestDetails(editGuestRequest)
    }

    @GetMapping("/allGuests")
    @ApiOperation(value = "Get Request For All Guest")
    fun allGuests() : MutableList<GuestDataResponse> = guestFacade.getAllGuests()

    @GetMapping(A_GUEST_API)
    @ApiOperation(value = "Get Request For A Guest")
    fun findAGuest(@PathVariable("guestId") guestId: String) : GuestDataResponse = guestFacade.findAGuest(guestId)


    @DeleteMapping(A_GUEST_API)
    @ApiOperation(value = "Delete A Guest")
    fun deleteAGuest(@PathVariable("guestId") guestId: String) : DefaultResponse{
        log.info("\n====Delete Guest Request ==== {}\n",guestId)
        return guestFacade.deleteAGuest(guestId)
    }

    companion object {
        val log = LoggerFactory.getLogger(GuestController::class.java)
    }
}