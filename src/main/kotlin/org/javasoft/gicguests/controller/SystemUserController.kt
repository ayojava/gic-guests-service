package org.javasoft.gicguests.controller

import org.javasoft.gicguests.api.LOGIN_API
import org.javasoft.gicguests.api.SYSTEM_USER_V1_API
import org.javasoft.gicguests.facade.SystemUserFacade
import org.javasoft.gicguests.payload.CreateSystemUserRequest
import org.javasoft.gicguests.payload.CreateSystemUserResponse
import org.javasoft.gicguests.payload.LoginSystemUserResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(SYSTEM_USER_V1_API)
class SystemUserController(@Autowired val systemUserFacade: SystemUserFacade) {

    @PostMapping("/createSystemUser")
    fun createSystemUser(@RequestBody createSystemUserRequestPayload: CreateSystemUserRequest) : CreateSystemUserResponse {
        log.info("====Create SystemUser Payload ==== {}",createSystemUserRequestPayload)
        return systemUserFacade.createSystemUser(createSystemUserRequestPayload)
    }

    @GetMapping(LOGIN_API)
    fun login(@PathVariable("userName") userName: String,@PathVariable("password") password: String): LoginSystemUserResponse {
        log.info("==== UserName :: {} , ==== Password :: {} ",userName,password)

        //log.info("====LoginSystemUser Response ==== {}",loginSystemUserResponse.toString())
        return systemUserFacade.loginSystemUser(userName, password)
    }

    companion object {
        val log = LoggerFactory.getLogger(SystemUserController::class.java)
    }
}