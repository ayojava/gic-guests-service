package org.javasoft.gicguests.controller

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.javasoft.gicguests.api.GUEST_V1_API
import org.javasoft.gicguests.api.UTIL_V1_API
import org.javasoft.gicguests.facade.EmailFacade
import org.javasoft.gicguests.facade.ProgramFacade
import org.javasoft.gicguests.payload.NewGuestResponse
import org.javasoft.gicguests.payload.NewProgramRequest
import org.javasoft.gicguests.payload.NewProgramResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(UTIL_V1_API)
@Api(value ="UtilController" ,description = "Set of endpoints for Utility.")
class UtilController{

    @Autowired
    lateinit var programFacade: ProgramFacade

    @Autowired
    lateinit var emailFacade: EmailFacade

    @PostMapping("/createProgram")
    @ApiOperation(value = "Post Request For New Program" , response =NewProgramResponse::class )
    fun createProgram(@ApiParam("NewProgramRequest Param") @RequestBody newProgramRequest: NewProgramRequest): NewProgramResponse =  programFacade.saveProgram(newProgramRequest)


    @GetMapping(value = "/allPrograms")
    fun listOfPrograms() : MutableList<NewProgramResponse> = programFacade.listOfPrograms()

    @GetMapping(value = "/allActivePrograms")
    fun listOfActivePrograms() : MutableList<NewProgramResponse> = programFacade.listOfActivePrograms()

//    @GetMapping(value = "/sendSMS")
//    fun testEmail() = emailFacade.sendSMS(44)

    companion object {
        val log = LoggerFactory.getLogger(UtilController::class.java)
    }
}

/*
* {
	"serviceName" :"Second Service",
	"serviceType":"Sunday Service"
}

{
	 "serviceName": "First Service",
    "serviceType": "Sunday Service"
}

"serviceName": "Communion Service",
    "serviceType": "Mid-Week Service
*
* */