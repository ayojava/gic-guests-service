package org.javasoft.gicguests.exception

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.dao.DataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler


@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class GenericExceptionHandler{

    @ExceptionHandler(DataAccessException::class)
    fun handleDataAccessException(dataAccessException: DataAccessException) : ResponseEntity<String> {
        return ResponseEntity("Something Bad Happened " , HttpStatus.INTERNAL_SERVER_ERROR)
    }
}