package org.javasoft.gicguests.entity

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate
import java.time.LocalDateTime
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@TypeAlias("PrayerRequestEntity")
@Document(collection="PrayerRequest")
data class PrayerRequestEntity(

        @Id
        var id: String? = null,

        var prayerRequest1 : String,

        var prayerRequest2 : String ,

        var prayerRequest3 : String,

        var prayerRequest4 : String
){
    override fun toString(): String{
        val mapper = ObjectMapper()
        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        return mapper.writeValueAsString(this)
    }
}



@TypeAlias("GuestEntity")
@Document(collection="Guest")
data class GuestEntity(
        @Id
        var id: String? = null,

        @NotEmpty
        var otherNames : String,

        @NotEmpty
        var surname :String,

        @NotEmpty
        var gender : String,

        @NotEmpty
        var maritalStatus : String,

        @NotEmpty
        var ageRange : String,

        @Email
        var email : String,

        @Email
        var altEmail : String,

        @NotEmpty
        var phoneNo : String,

        @NotEmpty
        var altPhoneNo : String,

        var birthday : LocalDate,

        @NotNull
        var attendanceDate : LocalDate,

        @NotEmpty
        val contactAddress : String,

        @NotEmpty
        val nearestBstop : String,

        @NotEmpty
        val category : String,

        val prayerRequestEntity: PrayerRequestEntity,

        var programEntity: ProgramEntity? = null
){
    override fun toString(): String{
        val mapper = ObjectMapper()
        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        return mapper.writeValueAsString(this)
    }
}



