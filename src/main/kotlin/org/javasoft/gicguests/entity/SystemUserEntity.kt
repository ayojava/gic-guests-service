package org.javasoft.gicguests.entity

import org.javasoft.gicguests.payload.CreateSystemUserResponse
import org.javasoft.gicguests.payload.LoginSystemUserResponse
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

@TypeAlias("UserEntity")
@Document(collection="SystemUser")
data class SystemUserEntity(
    @Id
    var id: String? = null,

    @NotEmpty
    var userName : String,

    @NotEmpty
    var password : String,

    @NotEmpty
    var firstName : String,

    @NotEmpty
    var lastName : String,

    @Email
    var email: String,

    @NotEmpty
    var phoneNo : String,

    var createDate : LocalDateTime
)

fun SystemUserEntity.toCreateSystemUserResponse() : CreateSystemUserResponse =
        CreateSystemUserResponse(this.userName,this.password,this.firstName,this.lastName,this.email,
                this.phoneNo,this.createDate)

fun SystemUserEntity.toLoginSystemUserResponse() : LoginSystemUserResponse =
    LoginSystemUserResponse(this.id!! , this.userName , this.firstName , this.lastName,this.email,this.phoneNo,
            createDate = this.createDate , dataAvailable = !(this.id.isNullOrBlank()))