package org.javasoft.gicguests.entity

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.mapping.Document

@TypeAlias("ProgramEntity")
@Document(collection="Program")
data class ProgramEntity(

        @Id
        var id: String? = null,

        var serviceName : String,

        var serviceType : String,

        var status : Boolean = true

){
    override fun toString(): String{
        val mapper = ObjectMapper()
        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        return mapper.writeValueAsString(this)
    }
}