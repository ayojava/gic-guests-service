package org.javasoft.gicguests.entity

import org.apache.commons.lang.StringUtils
import org.javasoft.gicguests.service.PENDING
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.mapping.Document

import java.time.LocalDateTime

@TypeAlias("SmsMessageEntity")
@Document(collection="SmsMessage")
data class SmsMessageEntity(

        @Id
        var id: String? = null,

        val phoneNo : String,

        val createDate : LocalDateTime = LocalDateTime.now(),

        val status : String= PENDING,

        val sentDate : LocalDateTime? = null,

        val guestId : String= StringUtils.EMPTY ,

        val smsMessage : String= StringUtils.EMPTY
)