package org.javasoft.gicguests.enums

enum class EbulkStatusEnum(val statusName: String , val statusDescription : String , val result : Boolean){

    SUCCESS("SUCCESS","Your message was received successfully" , true),

    INVALID_JSON("INVALID_JSON","The JSON format is not valid.", false);

    companion object {
        fun getEnum(statusName: String): EbulkStatusEnum =valueOf(statusName)
    }
}