package org.javasoft.gicguests.config

import org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.web.client.RestTemplate

@Configuration
class Beanconfig{

    @Bean
    fun getRestTemplate() = RestTemplate()

    @Bean
    @Scope(value = SCOPE_PROTOTYPE,proxyMode = ScopedProxyMode.TARGET_CLASS)
    fun validateConfig() : ValidateConfig = ValidateConfig();
}