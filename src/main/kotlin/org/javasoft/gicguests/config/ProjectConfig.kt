package org.javasoft.gicguests.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@ConfigurationProperties()
@PropertySource("file:./config/gicConfig.yml")
class EbulkSmsConfig{

    lateinit  var senderName : String

    lateinit var userEmail : String

    lateinit var authkey : String

    lateinit var ebulkSendSmsUrl : String

    lateinit var ebulkGetBalanceUrl : String

    lateinit var welcomeMessage : String
}