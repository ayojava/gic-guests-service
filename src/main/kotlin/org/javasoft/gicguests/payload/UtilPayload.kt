package org.javasoft.gicguests.payload

import io.swagger.annotations.ApiModel

@ApiModel(description = "New Program Request Data Class" )
data class NewProgramRequest(var serviceName : String , var serviceType : String)

data class NewProgramResponse(var id : String , var serviceName : String , var serviceType : String)