package org.javasoft.gicguests.payload

import com.fasterxml.jackson.annotation.JsonFormat
import org.javasoft.gicguests.entity.SystemUserEntity
import java.time.LocalDateTime
import java.util.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty


open class BaseSystemUser(var userName : String,var password : String="",var firstName : String ,var lastName : String,
                          var email: String,var phoneNo : String)

class CreateSystemUserRequest (userName : String, password : String,firstName : String,lastName : String,
                                           email : String, phoneNo : String):
        BaseSystemUser(userName,password,firstName, lastName, email, phoneNo)

fun CreateSystemUserRequest.toEntity() : SystemUserEntity =
        SystemUserEntity(userName=this.userName , password = this.password,firstName = this.firstName,
                lastName = this.lastName,email = this.email,phoneNo = this.phoneNo, createDate = LocalDateTime.now())

class CreateSystemUserResponse(userName : String, password : String,firstName : String,lastName : String,email : String, phoneNo : String ,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm")
        var createDate : LocalDateTime):
        BaseSystemUser(userName,password,firstName, lastName, email, phoneNo)

//data class LoginRequestPayload(var  userName: String , var password: String)

class LoginSystemUserResponse(var userId :String ,userName : String,firstName : String,
                              lastName : String,email : String, phoneNo : String ,var dataAvailable : Boolean = false,
                              @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm")
                              var createDate : LocalDateTime):
        BaseSystemUser(userName = userName,firstName = firstName, lastName = lastName, email = email, phoneNo = phoneNo)