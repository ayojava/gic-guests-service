package org.javasoft.gicguests.payload

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import java.lang.reflect.Constructor

@JsonPropertyOrder(value = *arrayOf("username", "apikey"))
//@JsonPropertyOrder("username","apikey")
data class Auth(
        @JsonProperty("username")
        var username : String,

        @JsonProperty("apikey")
        var apiKey: String
)

data class SmsMessage(
        @JsonProperty("sender")
        var senderName : String ,

        @JsonProperty("messagetext")
        var messageText : String ,

        @JsonProperty("flash")
        val flash : String? = "0"
)

data class RecipientMsg(
        @JsonProperty("msidn")
        var msidn :String ,

        @JsonProperty("msgid")
        var msgId : String
)

data class Recipients(
        @JsonProperty("gsm")
        var recipientMsgs: MutableList<RecipientMsg>
)

data class SmsRequest(
        @JsonProperty("auth")
        var auth: Auth,

        @JsonProperty("message")
        var smsMessage: SmsMessage,

        @JsonProperty("recipients")
        var recipients: Recipients,

        @JsonProperty("dndsender")
        var dndSender: Int = 1
)

data class EbulkSmsRequest(
        @JsonProperty("SMS")
        var smsRequest : SmsRequest
)


data class EbulkSmsResponse(

        @JsonProperty("response")
        var statusMsg : StatusMsg
)

data class StatusMsg(

        @JsonProperty("status")
        var status:String ="",

        @JsonProperty("totalsent")
        var totalSent:String ="",

        @JsonProperty("cost")
        var cost : String=""
)