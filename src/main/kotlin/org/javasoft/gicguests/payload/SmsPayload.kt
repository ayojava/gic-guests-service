package org.javasoft.gicguests.payload

import io.swagger.annotations.ApiModelProperty
import org.javasoft.gicguests.entity.SmsMessageEntity
import org.javasoft.gicguests.service.PENDING
import java.time.LocalDateTime

data class SmsRequestPayload(
        @ApiModelProperty(notes = "Phone number", required =true)
        val phoneNumber:String ,

        @ApiModelProperty(notes = "SMS Message", required =true)
        val smsMessage : String){

    companion object {
        fun createSmsMessageEntity(payload: SmsRequestPayload) : SmsMessageEntity =
                SmsMessageEntity(phoneNo = payload.phoneNumber,smsMessage = payload.smsMessage)
    }
}



data class SmsResponsePayload(var smsId : String,var createDate : LocalDateTime){
    companion object {
        fun createSmsResponsePayload(smsMessageEntity: SmsMessageEntity) : SmsResponsePayload =
                SmsResponsePayload(smsId = smsMessageEntity.id!!, createDate=smsMessageEntity.createDate!! )
    }
}

//data class GuestPayload(var firstName : String,var lastName :String,var gender : String,var ageRange : String ,
//                        var email : String ,var phoneNo : String,var altPhoneNo : String,var StreetNo : String,
//                        var bustop : String ,var area : String,var district : String,var state :String)


//