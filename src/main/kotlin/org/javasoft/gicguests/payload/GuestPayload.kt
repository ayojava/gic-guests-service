package org.javasoft.gicguests.payload

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import io.swagger.annotations.ApiModel
import org.apache.commons.lang.StringUtils
import org.javasoft.gicguests.entity.GuestEntity
import org.javasoft.gicguests.entity.PrayerRequestEntity
import org.javasoft.gicguests.entity.SmsMessageEntity
import org.javasoft.gicguests.service.PENDING
import org.javasoft.gicguests.toInternationalFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

@ApiModel(description = "New Guest Request Data Class" )
data class NewGuestRequest(var bioData: BioData, var prayerData: PrayerData,var programData: ProgramData,var hash : String ){
    override fun toString(): String{
        val mapper = ObjectMapper()
        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        return mapper.writeValueAsString(this)
    }
}

@ApiModel(description = "Edit Guest Request Data Class" )
data class EditGuestRequest(var guestId: String ,var bioData: BioData, var prayerData: PrayerData,var programData: ProgramData,
                            var hash : String , var resendSMS : Boolean){
    override fun toString(): String{
        val mapper = ObjectMapper()
        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        return mapper.writeValueAsString(this)
    }
}

data class DefaultResponse(var responseCode : String= StringUtils.EMPTY, var responseDesc : String= StringUtils.EMPTY,var dataAvailable : Boolean= true,
                            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm") var createDate : LocalDateTime = LocalDateTime.now())

@ApiModel(description = "All Guest Response Data Class" )
data class GuestDataResponse(var guestId: String ,var bioData: BioData, var prayerData: PrayerData,var programData: ProgramData,var hash : String ){
    override fun toString(): String{
        val mapper = ObjectMapper()
        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        return mapper.writeValueAsString(this)
    }
}



data class BioData(var surname : String , var otherNames :String , var gender :String , var maritalStatus :String,
                   var phoneNo : String , var altPhoneNo :String , var email : String , var altEmail :String,
                   var birthday : Date , var ageRange :String , var contactAddress :String , var nearestBstp : String,
                   var category : String)



data class ProgramData(var serviceName : String,var programDate : Date  , var serviceType : String)

data class PrayerData(var prayerRequest1 : String , var prayerRequest2 : String , var prayerRequest3 : String,
                      var prayerRequest4 : String )

data class NewGuestResponse(var responseCode : String= StringUtils.EMPTY, var responseDesc : String= StringUtils.EMPTY,var dataAvailable : Boolean= true,
                            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm") var createDate : LocalDateTime = LocalDateTime.now(),
                            var guestId : String)

fun NewGuestRequest.toClearString() : String {

    val(bioData,prayerData,programData) = this

    val(prayerRequest1 ,prayerRequest2,prayerRequest3,prayerRequest4) = prayerData

    val(surname,otherNames,gender,maritalStatus,phoneNo,altPhoneNo,email,altEmail,_,ageRange,contactAddress,
            nearestBstop,category) = bioData

    val(churchService,_,programName) = programData

    val bioDataString = surname + otherNames + gender + maritalStatus + phoneNo + altPhoneNo + email + altEmail + ageRange + contactAddress + nearestBstop + category

    val prayerDataString = prayerRequest1 + prayerRequest2 + prayerRequest3 + prayerRequest4

    val programDataString  = churchService + programName

    return bioDataString + prayerDataString + programDataString
}

fun NewGuestRequest.toEntity() : Pair<GuestEntity,  SmsMessageEntity>{

    val(bioData,prayerData,programData ) = this

    val(prayerRequest1 ,prayerRequest2,prayerRequest3,prayerRequest4) = prayerData

    val(surname,otherNames,gender,maritalStatus,phoneNo,altPhoneNo,email,altEmail,birthday,ageRange,contactAddress,
            nearestBstop,category) = bioData

    val smsMessageEntity = SmsMessageEntity(phoneNo = phoneNo.toInternationalFormat(),status = PENDING)


    val prayerRequestEntity = PrayerRequestEntity(prayerRequest1 = prayerRequest1 , prayerRequest2 = prayerRequest2,
            prayerRequest3 = prayerRequest3, prayerRequest4 = prayerRequest4)

    val guestEntity = GuestEntity(otherNames = otherNames,surname = surname,gender = gender,
            maritalStatus = maritalStatus,phoneNo = phoneNo ,altPhoneNo = altPhoneNo , email = email,
            altEmail = altEmail,birthday = birthday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),ageRange = ageRange ,contactAddress = contactAddress,
            nearestBstop = nearestBstop,category = category ,prayerRequestEntity = prayerRequestEntity,
            attendanceDate = programData.programDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
    return Pair(guestEntity,smsMessageEntity)
}